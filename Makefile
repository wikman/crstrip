.PHONY: build install-local

BUILD-DIR = _build
BUILD-BIN = $(BUILD-DIR)/crstrip

CC = gcc
CFLAGS = -std=c99 -O2

build:
	mkdir -p $(BUILD-DIR)
	$(CC) $(CFLAGS) -o $(BUILD-BIN) src/crstrip.c

install-local: build
	$(eval LOCAL-BIN := $(shell echo "$$HOME")/.local/bin)
	@echo "Installing to $(LOCAL-BIN)"
	mkdir -p $(LOCAL-BIN)
	cp $(BUILD-BIN) $(LOCAL-BIN)
