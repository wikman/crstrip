/**
 * @file crstrip.c
 * 
 * Entrypoint to the crstrip program.
 */

#include <errno.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>


static int write_from_buf(int output_fd, const char *buf, size_t len)
{
    // It might happen that we cannot write everything at once
    ssize_t ret;
    ssize_t to_write = len;
    while (to_write > 0) {
        ret = write(output_fd, buf, (size_t) to_write);
        if (ret <= 0) {
            return errno;
        }
        to_write -= ret;
    }
    return 0;
}


int crstrip(size_t init_bufsize, int input_fd, int output_fd)
{
    int code;
    ssize_t ret;
    size_t bufsize = init_bufsize;
    size_t maxlen = bufsize - 1;
    size_t curpos = 0;
    char *buf = malloc(bufsize * sizeof(char));
    if (buf == NULL)
        return ENOMEM;

    while (true) {
        size_t remaining = maxlen - curpos;
        ret = read(input_fd, &buf[curpos], remaining);
        if (ret < 0) {
            free(buf);
            return errno;
        }
        if (ret == 0) {
            // EOF reached, break and print out the rest at the bottom
            break;
        }

        size_t i = curpos;
        curpos += ret;
        while (i < curpos) {
            bool do_buffer_shuffle = false;

            // If we got a newline char, then we print out up until that point
            if (buf[i] == '\n') {
                code = write_from_buf(output_fd, buf, i + 1);
                if (code != 0) {
                    free(buf);
                    return code;
                }

                i += 1; // move past the newline
                do_buffer_shuffle = true;
            } else if (buf[i] == '\r') {
                i += 1; // move past the carriage return
                do_buffer_shuffle = true;
            } else {
                i += 1; // just record this value
            }

            if (do_buffer_shuffle) {
                // Strip what we previously had in the buffer
                for (size_t j = 0; j < (curpos - i); j++)
                    buf[j] = buf[i + j];

                curpos = curpos - i;
                i = 0;
            }
        }

        if (curpos == maxlen) {
            // out of memory, need a bigger buffer
            bufsize *= 2;
            char *new_buf = malloc(bufsize * sizeof(char));
            if (new_buf == NULL) {
                free(buf);
                return ENOMEM;
            }

            for (size_t j = 0; j < curpos; j++)
                new_buf[j] = buf[j];

            free(buf);
            buf = new_buf;
            maxlen = bufsize - 1;
        }
    }

    // By the above, we will have no CRs left in the buffer, so just print it
    code = write_from_buf(output_fd, buf, curpos);
    free(buf);
    return code;
}

int main(int argc, char *argv[])
{
    (void) argc;
    (void) argv;

    int fd_stdin = 0;
    int fd_stdout = 1;

    return crstrip(1024, STDIN_FILENO, STDOUT_FILENO);
}
